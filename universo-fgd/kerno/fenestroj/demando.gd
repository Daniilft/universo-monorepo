extends "res://kerno/fenestroj/tipo_c1.gd"
"""
форма для стандартизации простого вопроса, требующего ответа да или нет
заголовок - tipo_c/HBox_buttons/menuo_name.text
текст вопроса - grafitio.text
Ответ возвращается через сигнал. Перед вызовом подписываемся на сигнал resulto
В обработке смотрим параметр resulto 
Пример использования - "res://blokoj/administri/scenoj/main_administri.tscn"
"""

signal resulto(resulto) # вызывается при нажатии кнопки или закрытии окна

var signalo # отправлен ли сигнал
var form # поля для идентификации - откуда был вызов
var data # данные, для хранение

func _on_BJes_pressed():
	emit_signal("resulto", true)
	signalo = true
	self.set_visible(false)


func _on_demando_about_to_show():
	signalo = false


func _on_demando_popup_hide():
	if !signalo:
		emit_signal("resulto", false)


func _on_BNe_pressed():
	emit_signal("resulto", false)
	signalo = true
	self.set_visible(false)
