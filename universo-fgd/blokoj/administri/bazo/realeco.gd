extends "res://blokoj/administri/skriptoj/wablono.gd"


var main_node = null #вызывающая нода
var shangxi = false # признак создания или добавления

func _on_delete_pressed():
	"""
	Удаляем редактируемую запись с сервера
	"""
	print('=== надо бы удалить запись с сервера')
	pass # Replace with function body.


func krei_grid(parametro, app_node):
	"""
	создаём строки редактирования
	"""
	main_node = app_node
	purigi_grid()
	var grid = get_node("VBox/body_texture/ScrollContainer/VBoxContainer/GridContainer")
	grid.set_columns(2)
	if parametro:
		objekto = parametro.duplicate()
		shangxi = true
	var kodo = ''
	var nomo = ''
	var priskribo = ''
	if objekto:
		kodo = objekto['node']['kodo']
		nomo = objekto['node']['nomo']['json']
		priskribo = objekto['node']['priskribo']['json']
	aldoni_horizontalo2('Код:', 1, kodo, 'kodo')
	aldoni_horizontalo2('Наименование:', 2, nomo, 'nomo')
	aldoni_horizontalo2('Описание:', 2, priskribo, 'priskribo')
	#где у нас в системе уже получены языки с сервера. Эта часть на 27.04.2021 ещё не доработана
#	print('== 1 == ',TranslationServer.get_loaded_locales())
#	print('== 2 == ',TranslationServer.get_locale())
#	print('== 3 == ',TranslationServer.get_locale_name())
	pass


func _on_VBox_visibility_changed():
	if not $VBox.visible:
		# удалить сцену из Title
		self.queue_free()


func _on_save_pressed():
	var grid = $VBox/body_texture/ScrollContainer/VBoxContainer/GridContainer
	var kodo = ''
	var nomo = ''
	var priskribo = ''
	for item in grid.get_children():
#		if item.name.findn('==Label')>=0:
#			pass
#		el
		if item.name == 'kodo':
			kodo = item.text
		elif item.name == 'nomo':
			nomo = item._savi_datumoj()
		elif item.name == 'priskribo':
			priskribo = item._savi_datumoj()
	var query = main_node.QueryAdmini.new()
	if shangxi:
		Net.send_json(query.shanghi_realeco(objekto['node']['uuid'],
			kodo, nomo, priskribo))
	else:
		Net.send_json(query.aldoni_realeco(kodo, nomo, priskribo))
	# закрыть окно
	close()
	# обновить список загруженных объектов
	main_node.renovigi_modulo('UniversoRealeco')
