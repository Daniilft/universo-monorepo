extends TextureButton

func _ready():
	if Global.gramofono_paused:
		_on_pauzo_pressed()
		get_node("/root/Title/gramofono/regpanelo/fono/disco").set_visible(false)

#при клике на кнопку pauzo - она скрывается
#play отображается
#воспроизведение останавливается
func _on_pauzo_pressed():
	get_node("/root/Title/gramofono/regpanelo/fono/pauzo").set_visible(false)
	get_node("/root/Title/gramofono/regpanelo/fono/play").set_visible(true)
	get_node("/root/Title/gramofono").stream_paused = true

#если при наведении мышки клик не совершён и указатель выведен из 
#области pauzo, скрывается pauzo и отображается disco
func _on_pauzo_mouse_exited():
	get_node("/root/Title/gramofono/regpanelo/fono/pauzo").set_visible(false)
	get_node("/root/Title/gramofono/regpanelo/fono/disco").set_visible(true)
