extends Object
# Здесь будем хранить всё для запросов к бэкэнду по блоку "profilo"

# Необходимые заголовки
const headers = ["Content-Type: application/json"]


# Запрос на сохранение данных профиля
func save_profile_query(profile):
	if profile['uuid']:
		return JSON.stringify({ "query": "mutation { redaktuProfilo(retnomo: \"%s\", publikigo: true, uuid: \"%s\") { status message profilo { uuid retnomo } } }" % [profile['nickname'], profile['uuid']] })
	else:
		return JSON.stringify({ "query": "mutation { redaktuProfilo(retnomo: \"%s\", publikigo: true) { status message profilo { uuid retnomo } } }" % [profile['nickname']] })


# Запрос на сохранение данных профиля
func save_profile_ws(profile, id):
	var query
	if profile['uuid']:
		query = JSON.stringify({
			'type': 'start',
			'id': '%s' % id,
			'payload': { "query": "mutation { " +
			"redaktuProfilo(retnomo: \"%s\", publikigo: true, uuid: \"%s\") { status message profilo { uuid retnomo } } }" % [profile['nickname'], profile['uuid']] }})
	else:
		query = JSON.stringify({
			'type': 'start',
			'id': '%s' % id,
			'payload': { "query": "mutation { " +
			"redaktuProfilo(retnomo: \"%s\", publikigo: true) { status message profilo { uuid retnomo } } }" % [profile['nickname']] }})
	if Global.logs:
		print('=== save_profile_ws == ',query)
	return query
