extends Button


const QueryObject = preload("queries.gd")

# Объект HTTP запроса
var request
# Объект с данными для запросов
var q


# Вызывается при завершении обработки запроса авторизации к бэкэнду
# warning-ignore:unused_argument
# warning-ignore:unused_argument
func login_request_complete(result, response_code, headers, body):
	# print('=== result ===', result)
	# print('=== response_code ===', response_code)
	# print('=== headers ===', headers)
	# print('=== body ===', body)
	var resp = body.get_string_from_utf8()
	print('=== resp ===', resp)
	var test_json_conv = JSON.new()
	test_json_conv.parse(resp)
#	var parsed_resp: Dictionary = test_json_conv.get_data()
	var parsed_resp = test_json_conv.get_data()
	
	# Если ответ от бэкэнда содержит данные
	#if parsed_resp.has('data'):
	if parsed_resp != null:
		# true, если авторизован
		Global.status = parsed_resp['data']['ensaluti']['status']
		
		var message = parsed_resp['data']['ensaluti']['message']
		
		# Если не авторизован, выводим сообщение, которое вернул бэкэнд
		if !Global.status:
			message = tr("Wrong username or password")
			$"../message".set_visible(true)
			$"../message".set_mytext(message)
		# Если авторизован
		else:
			# Сюда сложим куки, которые прислал сервер в заголовках ответа
			var cookies = PackedStringArray()
			# Вытаскиваем токен, из ответа
			var token = parsed_resp['data']['ensaluti']['token']
			# Вытаскиваем csrf токен, из ответа
			var csrfToken = parsed_resp['data']['ensaluti']['csrfToken']
			# Вытаскиваем id пользователя из ответа
			Global.id = parsed_resp['data']['ensaluti']['uzanto']['objId']
			
			# Заполняем заголовки для следующих запросов к бэкэнду 
			Global.backend_headers.append("Referer: "+Net.http+"://"+Net.url_server+"/api/v1.1/")
			Global.backend_headers.append("X-Auth-Token: %s" % token)
			Global.backend_headers.append("X-CSRFToken: %s" % csrfToken)
			
			# Вытаксиваем куки из заголовков ответа
			for h in headers:
				if h.to_lower().begins_with('set-cookie'):
					cookies.append(h.split(':', true, 1)[1].strip_edges().split("; ")[0])
			# Куки так же сохраняем в заголовках для последующих запросов
			Global.backend_headers.append("Cookie: %s" % "; ".join(cookies))
			
			# поднимаем соединение по вебсокету
			Net.connect_to_server()
			
			# Разрегистрируем обработчик сигнала request_completed (вызывается
			# по завершении HTTPRequest)
			request.disconnect('request_completed', Callable(self, 'login_request_complete'))
			# Регистрируем новый обработчик (для обработки ответа на запрос по никнейму)
			request.connect('request_completed', Callable(self, 'get_nickname_request_complete'))
			
			# Делаем запрос к бэкэнду для получения никнейма.
			# Ответ будет обрабатываться в функции get_nickname_request_complete
			#var error = request.request(q.get_URL_DATA(), Global.backend_headers, true, 2, q.get_nickname_query(Global.id))
			var error = request.request(q.get_URL_DATA(), Global.backend_headers, true, q.get_nickname_query(Global.id))
			
			# Если запрос не выполнен из-за какой-то ошибки
			# TODO: Такие ошибки наверное нужно как-то выводить пользователю?
			if error != OK:
				print('Error in GET (nickname) Request.')
	# Если ответ от бэкэнда не содержит данные, которые мы ожидаем, выводим всё тело ответа
	# TODO: Такие ошибки наверное нужно как-то выводить пользователю?
	else:
		print(resp)


# Вызывается при завершении обработки запроса по никнейму к бэкэнду
# warning-ignore:unused_argument
# warning-ignore:unused_argument
# warning-ignore:unused_argument
func get_nickname_request_complete(result, response_code, headers, body):
	var resp = body.get_string_from_utf8()
	var test_json_conv = JSON.new()
	test_json_conv.parse(resp)
	var parsed_resp = test_json_conv.get_data()

	# Если ответ от бэкэнда содержит данные
	if parsed_resp.has('data'):
		# Если никнейм не задан
		print('===parsed_resp=',parsed_resp)
		if len(parsed_resp['data']['profilo']['edges']) == 0:
			# Загружаем сцену с данными профиля для ввода никнейма
# warning-ignore:return_value_discarded
			$"../../../../../Popup_profilo".popup()
			$"../../../../../Popup_profilo/profilo1/VBox".set_visible(true)#get_tree().change_scene_to_file('res://blokoj/profilo/profilo.tscn')
#			$CanvasLayer/UI/Popup_profilo/profilo1/VBox.set_visible(true)
		# Если получили никнейм
		else:
			# Сохраняем данные в глобальном объекте
			Global.nickname = parsed_resp['data']['profilo']['edges'][0]['node']['retnomo']
			Global.nickname_uuid = parsed_resp['data']['profilo']['edges'][0]['node']['uuid']
			# Запрашиваем управляемые объекты пользователя в параллельных мирах 
			# и где они находятся для последующей загрузки космоса или станции

			# Открываем сцену с видом космостанции
# warning-ignore:return_value_discarded
			get_tree().change_scene_to_file('res://blokoj/kosmostacioj/Kosmostacio.tscn')
	# Если ответ от бэкэнда не содержит данные, которые мы ожидаем, выводим всё тело ответа
	# TODO: Такие ошибки наверное нужно как-то выводить пользователю?
	else:
		print(resp)


# Вызывается при нажатии кнопки login
func _pressed():
	if Global.server:
		pass
	else:
		var password = $"../your_password".text
		var login = $"../your_login".text.strip_edges()
		eniri_uzanto(login, password)


# вошел пользователь
func eniri_uzanto(login, password):
	# Создаём объект HTTP запроса
	request = HTTPRequest.new()
	# Создаём объект с данными для запросов.
	# Данные для запросов и сами запросы храним в queries.gd
	q = QueryObject.new()

	# Добавляем объект запроса в сцену
	add_child(request)
	# Регистрируем обработчик сигнала request_completed, который придёт по завершении запроса
	request.connect('request_completed', Callable(self, 'login_request_complete'))
	# Делаем запрос авторизации к бэкэнду
	# Ответ будет обрабатываться в функции login_request_complete
	print('==== q.get_URL_AUTH() == ', q.get_URL_AUTH())
	print('==== Global.backend_headers == ', Global.backend_headers)
	print('==== q.auth_query(login, password) == ', q.auth_query(login, password))
	#var error = request.request(q.get_URL_AUTH(), Global.backend_headers, true, 2, q.auth_query(login, password))
	var error = request.request(q.get_URL_AUTH(), Global.backend_headers, true, q.auth_query(login, password))
	
	# Если запрос не выполнен из-за какой-то ожибки
	# TODO: Такие ошибки наверное нужно как-то выводить пользователю?
	if error != OK:
		print('Error in Auth Request.')


# вошел сервер
func eniri_server():
	print('Вошли как сервер')
	var config = ConfigFile.new()
	var err = config.load("res://settings.cfg")
	if err != OK:
		Global.server = false
		return 1
		
	var password = config.get_value("aliro_server", "password")
	var login = config.get_value("aliro_server", "uzanto")
	Global.kubo = config.get_value("kosmo", "kubo",1)
	Global.realeco = config.get_value("kosmo", "realeco",2)
	# Создаём объект HTTP запроса
	request = HTTPRequest.new()
	# Создаём объект с данными для запросов.
	# Данные для запросов и сами запросы храним в queries.gd
	q = QueryObject.new()
	
	# Добавляем объект запроса в сцену
	add_child(request)
	# Регистрируем обработчик сигнала request_completed, который придёт по завершении запроса
	request.connect('request_completed', Callable(self, 'login_request_complete'))
	# Делаем запрос авторизации к бэкэнду
	# Ответ будет обрабатываться в функции login_request_complete
	var error = request.request(q.get_URL_AUTH(), Global.backend_headers, true, 2, q.auth_query(login, password))
	
	# Если запрос не выполнен из-за какой-то ожибки
	# TODO: Такие ошибки наверное нужно как-то выводить пользователю?
	if error != OK:
		print('Error in Auth Request.')

