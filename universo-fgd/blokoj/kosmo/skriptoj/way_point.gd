extends Node3D
var cam #камера
var offset #сдвиг кнотрол-нод от нуля. Почему-то без него никак, хотя должно бы
var right_mouse_pressed = false #флаг правой мыши
@export var z_away = 100 # насколько глубоко\далеко будет точка пути от экрана
@export var colision_radius = 2.0 #радиус, на который надо подлететь к точке пути, чтобы она обнулилась

var active = false # есть ли точка пути, или ничего не указано

# warning-ignore:unused_signal
signal new_way_point(position) # сигнал кораблю, что пора лететь в точку position

func _ready():
	cam = get_tree().get_root().get_camera_3d() # получаем камеру
	offset =Vector2($TextureRect.size/2) # получаем смещение контрол-нод в центр
	$Area3D/CollisionShape3D.shape.radius = colision_radius #задаём радиус обнуления вейпойнта.

# warning-ignore:unused_argument
func _input(event):
#	if Input.is_action_just_pressed("left_click"):
#		if event is InputEventMouseButton and event.doubleclick:
#			emit_signal("new_way_point",cam.project_position(get_viewport().get_mouse_position(),z_away),null) #Если кнопка нажата, то бросаем лучь из камеры на глубину z_away и получаем точку, тут же устанавливаем новый вейпойнт с координатами
	pass
			
# warning-ignore:unused_argument
func _physics_process(delta):
	$TextureRect.position = cam.unproject_position(position) - offset #Эта секретная магия выводит ноду контрол именно там, где находится объект в зд-мире. Очень полезно.

# warning-ignore:unused_argument
func _on_Area_body_entered(body): #Если кто-то влетел в область вейпойнта
	set_active(false) #выключаем вейпойнт.

func set_active(on):
	active = on
	$TextureRect.visible = on
	if !on:
		position = Vector3.ZERO #сбрасываем в ноль выключенный вейпойнт. Потому что если этого не делать, то после того, как новый вейпйнт будет создан на том же месте, события "area_entered" не произойдёт, ибо никто из области и не выходил и корабль полетит дальше, оставив ваейпойнт позади

func set_way_point(position): #ставим точку вейпойнта.
	set_active(false)#сбрасываем вейпойнт
	position = position #ставим точку вейпойнта в полученные координаты.
	set_active(true) #активируем вейпойнт (включаем иконку)
	

# warning-ignore:unused_argument
func _on_VisibilityNotifier_camera_entered(camera): # Так как контрол-ноды в зд-мире (а они изначально 2д) видны не только напрямую, но и если обернуться на 180 градусов, то проверяем видимость именно зд -объекта. И только тогда рисум контрол-ноду. Если вейпойнт существует.
	if active:
		$TextureRect.visible = true

# warning-ignore:unused_argument
func _on_VisibilityNotifier_camera_exited(camera):
	$TextureRect.visible = false
