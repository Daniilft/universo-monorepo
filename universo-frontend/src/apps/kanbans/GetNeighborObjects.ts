const GetNeighborObjects = (child) => {
  const parent = child.getParentSizer();
  const children = parent.getElement('items');
  const childIndex = children.indexOf(child);
  return [children[childIndex - 1], children[childIndex + 1]];
};

export default GetNeighborObjects;
