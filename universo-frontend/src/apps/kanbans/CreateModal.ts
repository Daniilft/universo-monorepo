import { EndlessCanvas } from '../spaces-2d/EndlessCanvas';
import CreateRequestModal from './CreateRequestModal';

type ButtonAction = (textInput: string) => void;

export function createModal(
  scene: EndlessCanvas,
  onSubmit?: ButtonAction,
  defaultContentText?: string,
  titleText?: string,
  cancelButtonText?: string,
  confirmButtonText?: string,
) {
  scene.isInputMode = true;
  // Функция для создания и отображения модального окна
  const modal = CreateRequestModal(
    scene,
    onSubmit,
    defaultContentText,
    titleText,
    cancelButtonText,
    confirmButtonText,
  )
    .setPosition(scene.cameras.main.width / 2, scene.cameras.main.height / 2)
    .layout()
    .modalPromise({
      manualClose: true,
      duration: {
        in: 500,
        out: 500,
      },
    });

  return modal;
}
