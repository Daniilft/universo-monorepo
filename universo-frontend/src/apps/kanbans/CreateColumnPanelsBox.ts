import { COLOR_LIGHT, COLOR_PRIMARY, COLOR_DARK } from './Const';
import CreateColumnPanel from './CreateColumnPanel';
import AddDragDropColumnPanelBehavior from './AddDragDropColumnPanelBehavior';
import ObjectWrapper from 'src/utils/objectWrapper';
import { PanelsBoxType } from 'src/types/kanban_types';
import { EventBus, Events } from 'src/boot/eventBus';
import { EndlessCanvas } from '../spaces-2d/EndlessCanvas';
import { getAllChildrens } from '../utils/GetAllChildrens';

const CreateColumnPanelsBox = (scene: EndlessCanvas, board: ObjectWrapper) => {
  const config = {
    orientation: 'x',
    space: {
      left: 10,
      right: 10,
      top: 10,
      bottom: 10,
    },
  };
  //@ts-ignore

  const columnPanelsBox = scene.rexUI.add.sizer(config) as PanelsBoxType;

  const panelsBoxMap = new Map();
  board.childrens.forEach((column: ObjectWrapper) => {
    const columnPanel = CreateColumnPanel(scene, column);
    panelsBoxMap.set(column.uuid, columnPanel);
    columnPanelsBox.add(columnPanel, { proportion: 0, expand: true });
  });

  const box = new AddDragDropColumnPanelBehavior(columnPanelsBox);
  EventBus.$on(Events.Delete, (uuid, object) => {
    if (panelsBoxMap.has(uuid)) {
      //@ts-ignore
      box.panelsBox.remove(panelsBoxMap.get(uuid), true);
      box.panelsBox.layout();
    }
  });

  EventBus.$on(Events.Create, (parentUuid, object) => {
    if (board.uuid !== parentUuid) {
      return;
    }
    const columnPanel = CreateColumnPanel(scene, object);

    scene.cameras.getCamera('ui').ignore(getAllChildrens(columnPanel));
    //@ts-ignore
    panelsBoxMap.set(object.uuid, columnPanel);

    box.addPanel(columnPanel);
    box.panelsBox.getTopmostSizer().layout();
  });
  return box.panelsBox;
};

export default CreateColumnPanelsBox;
